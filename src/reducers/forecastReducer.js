let initialState = {
    weatherForecast: {
        city:'Lyon',
        localtime:'26/01/2021 19:00/00',
        weather_description:'Clear night',
        temperature:'3°C',
        weather_icon:null,
        wind_speed:'6km/h',
        humidity:'66%'
    },
    loader: false,
    city: 'Lyon',
}

export const forecastReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_FORECAST':
            return {
                ...state,
                weatherForecast:action.weatherForecast
            };
            //console.log(weatherForecast)
        case 'CHOSEN_CITY':
            return {
                ...state,
                city: action.value
            };
        default:
            return state;
    }
};

export default forecastReducer;