import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, Image } from 'react-native';

export function WeatherResult(props) {
    return (
        <View style={styles.background}>
            <View style={styles.primary_background}>
                <View style={styles.forecast}>
                    <Image source={require('../../assets/adaptive-icon.png')} style={styles.icon}></Image>
                    <Text style={styles.weather_description}>{props.weatherForecast.weather_description}</Text>
                </View>
                <Text style={styles.weather_description}>{props.weatherForecast.temperature}</Text>
            </View>
            <View style={styles.secondary_background}>
                <Text style={styles.more_description}>{props.weatherForecast.wind_speed}</Text>
                <Text style={styles.more_description}>{props.weatherForecast.humidity}</Text>
            </View>
        </View>

    );
}

const mapStateToProps = (state) => {
    return {
        weatherForecast: state.forecastReducer.weatherForecast,
    }
}

export default connect(mapStateToProps)(WeatherResult)

const styles = StyleSheet.create({
    background: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    primary_background: {
        backgroundColor: '#FFFFFF',
        opacity: 0.80,
        borderRadius: 12,
        height: 270,
        width: 283,
        alignItems: 'center',
        justifyContent: 'center',

    },
    secondary_background: {
        backgroundColor: '#404491',
        borderRadius: 12,
        opacity: 1,
        height: 70,
        width: 230,
        marginTop: -35,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'nowrap'

    },
    more_description: {
        color: 'white',
        margin: 10,
        fontWeight: 'bold',
    },
    weather_description:{
        color:'#404491',
        fontWeight:'bold',
        fontSize:30
    },
    forecast:{
        flexDirection: 'row',
        flexWrap: 'nowrap',
        alignItems: 'center',
        marginTop:-60,
        marginBottom: 40
    },
    icon:{
        width: 80,
        height: 80
    }
});
