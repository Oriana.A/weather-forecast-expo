import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { fetchForecast } from "../actions/forecast";
import { connect } from 'react-redux';
import {SearchButton} from './SearchButton'

export const fetchWeatherForecast = (props) => {
    fetchForecast(props.city);
} 

export function ChooseCityInput(props) {
    
    return (
        <View style={styles.choose_city_container}>
            <TextInput style={styles.input} value={props.value} onChange={props.onChange}/>
            <SearchButton onPress={fetchWeatherForecast}/>
        </View>
    );
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchForecast
    }, dispatch)
}

export default connect(mapDispatchToProps)(ChooseCityInput)


const styles = StyleSheet.create({
    choose_city_container: {
        flexDirection: 'row'
    },
    input: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        opacity: 0.90,
        borderRadius: 12,
        marginTop: 40,
        width: 195,
        height: 60
    }
});
